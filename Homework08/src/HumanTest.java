import java.util.Scanner;

public class HumanTest {
    public static void main(String[] args) {
       /* Human[] humanArray = new Human[]
                {
                        new Human("Alex",77.5),
                        new Human("Bill",87.9),
                        new Human("Carl",56.3),
                        new Human("Douglas",100.5),
                        new Human("Evan",67.4),
                        new Human("Fred",120.6),
                        new Human("Gery",62.1),
                        new Human("Harry",96.6),
                        new Human("Isaak",62.8),
                        new Human("Jake",85.6)
                };*/
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\\n");
        Human[] humanArray = new Human[10];
        for (int i = 0; i < humanArray.length; i++) {
            System.out.print("Name: ");
            String name = sc.next();
            System.out.print("Weight: ");
            double weight = sc.nextDouble();
            System.out.println();
            humanArray[i] = new Human(name,weight);
        }
        Human.sortHumansByWeight(humanArray,0, humanArray.length - 1);
        for(Human human : humanArray){
            System.out.println(human);
        }

    }
}
