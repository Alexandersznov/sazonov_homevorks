public class Human {
    private String name;
    private double weight;

    public Human(String name, double weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * Метод сортировки массива объектов класса Human. Используется алгоритм сортировки выбором
     * @param array массив, который необходимо отсортировать
     * @param from индекс элемента, начиная с которого быдет производиться сортировка
     * @param to индекс элемента, по который будет производиться сортировка
     */
    public static void sortHumansByWeight(Human[] array, int from, int to){
        for (int i = from; i < to; i++) {
            int minIndex = i;
            for(int j = i+1; j<= to;j++){
                if(array[j].getWeight() < array[minIndex].getWeight()){
                    minIndex = j;
                }
            }
            if(minIndex != i){
                Human tmp = array[i];
                array[i] = array[minIndex];
                array[minIndex] = tmp;
            }
        }
    }

    @Override
    public String toString() {
        return "name: " + this.getName() + " " + "weight: " + this.getWeight();
    }
}
