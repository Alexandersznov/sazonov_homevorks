import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        int minDigit = 9;
        while(number !=- 1){
            while (number>0){
                int lastDigit = number % 10;
                if(lastDigit<minDigit){
                    minDigit = lastDigit;
                }
                number /= 10;
            }
            number = sc.nextInt();
        }
        System.out.println(minDigit);

    }
}