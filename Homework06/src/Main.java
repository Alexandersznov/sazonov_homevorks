public class Main {

    public static void main(String[] args) {
	// write your code here
        int[] array = {1,4,6,67,-5,67,8};
        int result1 = findInArray(array,8);
        int result2 = findInArray(array,10);
        System.out.println(result1);
        System.out.println(result2);

        double[] array2 = {1.5,0,67.3,0,0,56.4,7,0,789};
        zerosToEnd(array2);
        for (double a: array2) {
            System.out.print(a + " ");
        }

    }

    /**
     * Метод ищет число в массиве и возвращает индекс первого найденного элемента или -1
     * @param array массив,  в котором происходит поиск
     * @param number целое число, которое нужно найти
     * @return индекс найденного элемента или -1
     */
    public static int findInArray(int[] array, int number){
        for (int i = 0; i < array.length; i++) {
            if(array[i] == number){
                return i;
            }
        }
        return -1;
    }

    /**
     * Метод перемещает все значисые элементы влево, заменяя нулевые
     * @param array массив, в котором будет произведен сдвиг
     */
    public static void zerosToEnd(double[] array){
        double[] tempArray = new double[array.length];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if(array[i] !=0 ){
                tempArray[j] = array[i];
                j++;
            }
        }
        for (int i = j; i < array.length; i++) {
            tempArray[i] = 0;

        }
        for (int i = 0; i < array.length; i++) {
            array[i] = tempArray[i];
        }
    }
}
